﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareDev_GeometryChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the radius of the circle and press ENTER: ");
            var radius = Double.Parse(Console.ReadLine());
            Console.WriteLine("Please enter the x-coordinate of a point on the circle and press ENTER: ");
            var radPointX = Double.Parse(Console.ReadLine());
            Console.WriteLine("Please enter the y-coordinate of a point on the circle and press ENTER: ");
            var radPointY = Double.Parse(Console.ReadLine());
            Console.WriteLine("Please enter the x-coordinate of a point inside/on the circle and press ENTER: ");
            var innerPointX = Double.Parse(Console.ReadLine());
            Console.WriteLine("Please enter the y-coordinate of a point inside/on the circle and press ENTER: ");
            var innerPointY = Double.Parse(Console.ReadLine());
            var calcPoint = ComputePointOnCircle(radius, new Point(radPointX, radPointY), new Point(innerPointX, innerPointY));
            Console.WriteLine("The other point that lies on a line with the two given points and is located on the circle's edge is located at " + calcPoint.ToString());
            Console.ReadKey();
        }

        private static Point ComputePointOnCircle(double radius, Point radPoint, Point innerPoint)
        {
            // get variables for line equation
            double slope = (radPoint.yCoord - innerPoint.yCoord) / (radPoint.xCoord - innerPoint.xCoord);
            double lineIntercept = radPoint.yCoord - (slope * radPoint.xCoord);

            // calculate variables for quadratic formula
            double a = 1 + Math.Pow(slope, 2);
            double b = 2 * slope * lineIntercept;
            double c = Math.Pow(lineIntercept, 2) - Math.Pow(radius, 2);

            // solve both possibilities of quadratic formula
            double x1 = ((b * -1) + Math.Sqrt(Math.Pow(b, 2) - (4 * a * c))) / (2 * a);
            double x2 = ((b * -1) - Math.Sqrt(Math.Pow(b, 2) - (4 * a * c))) / (2 * a);

            // choose x coordinate that wasn't already given
            var x = (radPoint.xCoord == x1) ? x2 : x1;
            
            // solve for new y coordinate
            var y = (slope * x) + lineIntercept;

            return new Point(x, y);
        }

        private class Point
        {
            public Point(double xCoord, double yCoord)
            {
                this.xCoord = xCoord;
                this.yCoord = yCoord;
            }

            public double xCoord { get; set; }
            public double yCoord { get; set; }

            public override string ToString()
            {
                return "(" + this.xCoord + ", " + this.yCoord + ")";
            }
        }
    }
}
